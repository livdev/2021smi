<?php

namespace Database\Factories;

use App\Models\Aritcle;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AritcleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Aritcle::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $name=$this->faker->text(120);
        $slug=str_slug($name);
        return [
            'name'=>$name,
            'slug'=>$slug,
            'text_article'=>$this->faker->text(500)
        ];
    }


}
