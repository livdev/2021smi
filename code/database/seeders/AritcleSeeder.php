<?php

namespace Database\Seeders;

use App\Models\Aritcle;
use Illuminate\Database\Seeder;

class AritcleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Aritcle::factory()->times(200)->create();
    }
}
