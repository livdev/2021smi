<?php

namespace Database\Seeders;

use App\Models\Aritcle;
use Illuminate\Database\Seeder;
use Database\Seeders\AritcleSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->run(Aritcle::factory(500)->create());
    }
}
