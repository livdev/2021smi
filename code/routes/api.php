<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::post('/show/send',[\App\Http\Controllers\ShowController::class, 'addShow']);
Route::post('/like/send',[\App\Http\Controllers\LikeController::class, 'addLike']);
Route::post('/comment/send',[\App\Http\Controllers\CommentController::class, 'addComment']);
