<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [\App\Http\Controllers\AritcleController::class, 'indexhome'])->name('home');
Route::get('/articles', [\App\Http\Controllers\AritcleController::class, 'showall'])->name('allrecord');
Route::get('/articles/{aritcle:slug}', [\App\Http\Controllers\AritcleController::class, 'show'])->name('slug');
