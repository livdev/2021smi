@extends('front.app')

@section('title', 'Главная страница')

@section('content')
    <div class="wrapper_all">

        @foreach($articles as $article)
            <div class="item">

                <div class="title_item">
                    <a href="{{  url('/articles/'.$article->slug) }}">  {{$article->name}}</a>
                </div>

                <div class="img_item">
                    <img src="https://via.placeholder.com/150">
                </div>

                <div class="text_item">
                    {{substr($article->text_article, 0, 60)   }} ...
                </div>
                <div class="footer_items">
                    <div class="look">
                        <i class="fa fa-eye" aria-hidden="true"></i> {{ $article->getShow() }}
                    </div>
                    <div class="likeall" style="display: flex;flex-wrap: wrap">
                        <i class="fa fa-heart sendlike" aria-hidden="true"></i> <span id="showLike">{{ $article->getLike() }} </span>
                    </div>
                </div>
            </div>
        @endforeach


        {{ $articles->links() }}

        <div class="comment_wrapper">
            <div class="comment_items">

            </div>
        </div>
    </div>


@endsection

@section('mystyle')
    <style>
        .wrapper_all{
            max-width: 1024px;
            margin: auto;
        }
        .comment_wrapper{
            max-width: 1024px;
            margin: auto;
        }
        .item{
            width: 100%;
            padding: 10px;
        }
        .img_item{
            width: 100%;
            padding: 10px;
        }
        .img_item img{
            width: 100%;
            height: 200px;
        }
        .comment_items{
            display: flex;
            flex-wrap: wrap;
        }
    </style>
@endsection
