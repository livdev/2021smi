@extends('front.app')

@section('title', 'Главная страница')

@section('content')
    <div class="wrapper_new">
        @foreach($articles as $article)
            <div class="items">
                <div class="img_items">
                    <img src="https://via.placeholder.com/150">
                </div>
                <div class="title_items">
                    <a href="{{  url('/articles/'.$article->slug) }}">
                        {{substr($article->name, 0, 40)   }} ...</a>
                </div>
                <div class="сontent_items">
                    {{substr($article->text_article, 0, 60)   }} ...
                </div>
                <div class="footer_items">
                    <div class="look">
                        <i class="fa fa-eye" aria-hidden="true"></i> {{ $article->getShow() }}
                    </div>
                    <div class="like">
                        <i class="fa fa-heart" aria-hidden="true"></i> {{ $article->getLike() }}
                    </div>
                </div>

            </div>
        @endforeach

    </div>
@endsection
