@foreach($article->comments() as $itemComment)
    <div class="items">
        <div class="sub">
            {{$itemComment->subject}} (  {{$itemComment->created_at}})
        </div>
        <div class="text_val">
            {{$itemComment->body}}
        </div>
    </div>
@endforeach
