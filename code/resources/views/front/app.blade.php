<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>@yield('title')</title>
        <style>
            html{
                box-sizing: border-box;
                font-family: 'Roboto', sans-serif;
            }
            *,*::before,*::after{
                box-sizing: inherit;
                margin: 0px;
            }
            .head_css {
                background: white;
            }
            .wrapper_head{
                justify-content: center;
                display: flex;
                justify-content: space-between;
                max-width: 1024px;
                margin: auto;
            }
            .banner{
                width: 100%;
                height: 200px;
                background: #e2e8f0;
            }
            .wrapper_banner{
                position: relative;
                max-width: 1024px;
                margin: auto;
            }
            .h1_text{
                position: absolute;
                top: 60px;
                font-size: 44px;
            }
            .span_text{
                position: absolute;
                top: 120px;
                font-size: 23px;
            }
            .wrapper_new{
                display: flex;
                max-width: 1024px;
                margin: auto;
                margin-top: 20px;
                flex-wrap: wrap;
                justify-content: space-around;
                min-height: 90vh;
            }
            .items{
                width: 25% ;
                margin-left: 10px;
                margin-right: 10px;
                margin-top: 32px;
                box-shadow: 0 0 10px rgba(0,0,0,0.4);
            }
            .img_items{
                width: 100%;
            }
            .img_items img{
                width: 100%;
            }
            .title_items{
                width: 100%;
                font-size: 16px;
                padding: 10px;
            }
            .сontent_items{
                margin-top: 20px;
                width: 100%;
                font-size: 12px;
                padding: 10px;
            }
            .footer_items{
                padding: 10px;
                display: flex;
                flex-wrap: wrap;
                justify-content: space-between;
            }
            .like{
                cursor: pointer;
            }
            .footer{
                margin-top: 30px;
                width: 100%;
                height: 100px;
            }
            .wrapper_footer{
                display: flex;
                max-width: 1024px;
                margin: auto;
                margin-top: 20px;
                flex-wrap: wrap;
            }
            .logo_footer{
                width: 200px;
            }
        </style>


        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    </head>
    <body>
       <div class="head_css">
            <div class="wrapper_head">
                <div class="logo">
                    Тредиум
                </div>
                <div class="menu">
                    <a href="{{ route('home') }}">На главную</a>
                    <a href="{{ route('allrecord') }}">Каталог статей</a>
                </div>
            </div>
       </div>
        <div class="banner">
            <div class="wrapper_banner">
                <div class="h1_text">
                    Успеx
                </div>
                <div class="span_text">
                    Для молодыx и успешныx
                </div>
            </div>
        </div>

       @yield('content')



        <div class="footer">
            <div class="wrapper_footer">
                <div class="logo_footer">
                    <b>Тредиум</b>
                    <br>
                    <br>3001-3020 Dct
                </div>
                <div class="text_footer">
                    Блог<br>
                    <br> Блог о всем
                </div>
            </div>
        </div>
    </body>
    @yield('mystyle')
    @yield('myjs')

</html>
