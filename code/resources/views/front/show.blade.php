@extends('front.app')

@section('title', 'Новость-'.$article->name)



@section('content')
    <div class="wraper_item">
        <h1> {{ $article->name }}</h1>
        <div class="footer_items">
            <div class="look">
                <i class="fa fa-eye" aria-hidden="true"></i> {{ $article->getShow() }}
            </div>
            <div class="like" style="display: flex;flex-wrap: wrap">
                <i class="fa fa-heart sendlike" aria-hidden="true"></i> <span id="showLike">{{ $article->getLike() }} </span>
            </div>
        </div>
        <p > {{ $article->text_article }}</p>
    </div>
    <input type="hidden" class="val_id" value="{{$article->id}}">

    <div class="add_comment">
        <form id="add_comment">
            <div class="form_name">
                Тема <input type="text"  name="subject" class="subject" required>
            </div>
            <div class="form_name">
                Текст <textarea name="subject" class="textcomm" required ></textarea>
            </div>
            <input type="submit" id="sendBtn">
        </form>
    </div>

    <div class="list_comment">
        @foreach($article->comments() as $itemComment)
            <div class="items">
                <div class="sub">
                    {{$itemComment->subject}} (  {{$itemComment->created_at}})
                </div>
                <div class="text_val">
                    {{$itemComment->body}}
                </div>
            </div>
        @endforeach
    </div>

@endsection


@section('mystyle')
    <style>
        .wraper_item{
            max-width: 1024px;
            margin: auto;
        }
        .add_comment{
            max-width: 1024px;
            margin: auto;
            margin-top: 50px;
            margin-bottom: 50px;
        }

        .list_comment{
            max-width: 1024px;
            margin: auto;
            margin-top: 50px;
            margin-bottom: 50px;
        }
        .list_comment .items{
            width: 100%;
        }
    </style>
@endsection

@section('myjs')
    <script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"></script>
    <script>
        /**
         * Увеличиваем счетчик просмотров
         */
        setTimeout(function(){  let id_record=$('.val_id').val();
            $.ajax({
                type: "POST",
                url: "/api/show/send",
                data: {
                    'id': id_record
                },
                success: function(data) {
                    console.log(data);

                },
                error: function (data) {
                    console.log(data);
                }
            }); }, 5000);

        /**
         * Отпровляем like
         */
        $('.sendlike').on('click',function () {
            let id_record=$('.val_id').val();
            $.ajax({
                type: "POST",
                url: "/api/like/send",
                data: {
                    'id': id_record
                },
                success: function(data) {
                    console.log(data);
                    $('#showLike').html(data);
                   // document.getElementById('')
                },
                error: function (data) {
                    console.log(data);
                }
            });
        })


        /**
         *Отпровляем комментарий
         */
        $(document).on("submit","#add_comment",function () {
            event.preventDefault();
            $('#sendBtn').hide();
            let subject=$('.subject').val();
            let id_record=$('.val_id').val();
            let textcomm=$('.textcomm').val();
            $.ajax({
                type: "POST",
                url: "/api/comment/send",
                data: {
                    'id': id_record,
                    'subject':subject,
                    'textcomm':textcomm,
                },
                success: function(data) {
                    console.log(data);
                    $('#sendBtn').show();
                    $('.list_comment').html(data);
                    alert('Add comment');
                    // document.getElementById('')
                },
                error: function (data) {
                    $('#sendBtn').show();
                    alert('Errors ');
                    console.log(data);
                }
            });
        })
    </script>
@endsection
