<?php

namespace App\Http\Controllers;

use App\Models\Aritcle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AritcleController extends Controller
{


    public function indexhome(){
       // $articles=DB::table('aritcles')->orderBy('id','desc')->skip(0)->take(6)->get();
        $articles=Aritcle::query()->orderBy('id','desc')->skip(0)->take(6)->get();
        return view('front.index',['articles' => $articles]);
    }



    public function showall()
    {
        $articles=Aritcle::simplePaginate(10);
        return view('front.showall',['articles' => $articles]);
    }


    public function show(Aritcle $aritcle)
    {

        return view('front.show',['article' => $aritcle]);
    }


}
