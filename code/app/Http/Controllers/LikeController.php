<?php

namespace App\Http\Controllers;

use App\Models\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LikeController extends Controller
{
    public function addLike(Request $request)
    {
        $id=$request->input('id');
        DB::table('likes')->insert([
            'aritcle_id' => $id,
        ]);
        return DB::table('likes')->where('aritcle_id',$id)->count();
    }
}
