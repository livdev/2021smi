<?php

namespace App\Http\Controllers;

use App\Models\Aritcle;
use App\Models\Comment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{

    public function addComment(Request $request)
    {
        $id=$request->input('id');
        $subject=$request->input('subject');
        $textcomm=$request->input('textcomm');
        DB::table('comments')->insert([
            'aritcle_id' => $id,
            'subject' => $subject,
            'body' => $textcomm,
            'created_at'=>Carbon::now()
        ]);
        $aritcle=Aritcle::find($id);
        $html=view('front.ajaxshow',['article' => $aritcle])->render();
        return $html;
    }

}
