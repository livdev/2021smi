<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;


class Aritcle extends Model
{
    use HasFactory;

    public function getShow(){
        $seconds=15;
        $id_record=$this->id;
        $value = Cache::remember('article_'.$id_record, $seconds, function () use ($id_record) {
            return DB::table('shows')->where('aritcle_id',$id_record)->count();
        });
        return $value;
    }

    public function getLike(){
        $seconds=5;
        $id_record=$this->id;
        $value = Cache::remember('like_'.$id_record, $seconds, function () use ($id_record) {
            return DB::table('likes')->where('aritcle_id',$id_record)->count();
        });
        return $value;
    }

    public function comments(){
        return $this->hasMany(Comment::class,'aritcle_id','id')->get();
    }

}
